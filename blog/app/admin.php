<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\pengguna;

class admin extends Model
{
    protected $table='admin';
    protected $fillable=['nama','notlp','email','alamat','pengguna_id'];

     public function pengguna(){
    	return $this->belongsTo(pengguna::class);
    }
}
