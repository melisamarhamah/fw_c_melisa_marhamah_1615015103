<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\penulis;
use App\pembeli;


class buku extends Model
{
    protected $table='buku';
    protected $fillable=['judul','kategori_id','penerbit','tanggal'];

    public function kategori(){
    	return $this->belongsTo('App\kategori');
    }

    public function penulis(){
    	return $this->belongsToMany(penulis::class);
    }

    public function pembeli(){
    	return $this->belongsToMany(pembeli::class);
    }
}
