<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\pengguna;
use App\buku;

class pembeli extends Model
{
     protected $table='pembeli';
     protected $fillable=['nama','notlp','email','alamat','pengguna_id'];

     public function pengguna(){
     	return $this->belongsTo(pengguna::class);
     }
     public function buku(){
     	return $this->hasMany(buku::class);
     }
     public function getUsernameAttribute(){         
     	return $this->pengguna->username; } 
 
public function getPasswordAttribute(){         
	return $this->pengguna->password; } 
 
}