<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\pembeli;
use App\admin;

class pengguna extends Model
{
    protected $table='pengguna';
    protected $fillable=['username','password'];

    public $hidden = [
    'password','remember_token'];

    public function pembeli(){
    	return $this->hasOne(pembeli::class);
    }
    public function admin(){
    	return $this->hasOne(admin::class);
    }
}
