<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\admin;

class AdminController extends Controller
{
     public function awal(){
    	$admin=admin::all();
    	return view('admin.app',compact('admin'));
    }
    public function tambah(){
    	return view('admin.tambah');
    }

    public function simpan(Request $input){
    	$this->validate($input,array(
     'username' => 'required',
     'password' => 'required',
     'nama' => 'required',
     'notlp' => 'required',
     'email' => 'required',
     'alamat' => 'required',
));
            $admin = new admin();   
            $admin->nama = $input->nama;   
            $admin->notlp = $input->notlp;   
            $admin->email = $input->email;   
            $admin->alamat = $input->alamat;   
            $status = $admin->save();
            return redirect('admin');

    }
    public function edit($id){
    	$admin=admin::find($id);
    	return view('admin.edit')->with(array('admin'=>$admin));

    }
    public function update($id,Request $input){
    	$admin=admin::find($id);
    	$admin->nama=$input->nama;
    	$admin->notlp=$input->notlp;
    	$admin->email=$input->email;
    	$admin->alamat=$input->alamat;
        $admin->pengguna_id=$input->pengguna_id;
    	$status=$admin->save();
    	return redirect('admin')->with(['status'=>$status]);
    }
    public function hapus($id){
    	$admin=admin::find($id);
    	$admin->delete();
    	return redirect('admin');
    }
}
