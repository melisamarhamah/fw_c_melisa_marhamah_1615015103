<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\pembeli;
use App\pengguna;

class PembeliController extends Controller
{
      public function awal(){
    	$pembeli=pembeli::all();
    	return view('pembeli.App',compact('pembeli'));
    }
    public function tambah(){
    	return view("pembeli.tambah");
    }

    public function simpan(Request $input){
       $this->validate($input,array(
     'username' => 'required',
     'password' => 'required',
     'nama' => 'required',
     'notlp' => 'required',
     'email' => 'required',
     'alamat' => 'required',
));
            $pengguna = new pengguna();
            $pengguna->username = $input->username;
            $pengguna->password = $input->password;
            $pengguna->level=1;
            $pengguna->save();

            $pembeli = new pembeli();   
            $pembeli->nama = $input->nama;   
            $pembeli->notlp = $input->notlp;   
            $pembeli->email = $input->email;   
            $pembeli->alamat = $input->alamat;  
            $pembeli->pengguna_id = $pengguna->id; 
            $status = $pembeli->save();
            return redirect('pembeli');
    }
    public function edit($id){
    	$pembeli=pembeli::find($id);
    	return view('pembeli.edit')->with(array('pembeli'=>$pembeli));

    }
    public function update($id,Request $input){
    	$pembeli=pembeli::find($id);
    	$pembeli->nama=$input->nama;
    	$pembeli->notlp=$input->notlp;
    	$pembeli->email=$input->email;
    	$pembeli->alamat=$input->alamat;

        $pengguna = Pengguna::find($pembeli->pengguna_id);   
        $pengguna->username = $input->username;   
        $pengguna->password = $input->password;   
        $pengguna->level = "village";   

        
    	$status=$pembeli->save();
    	return redirect('pembeli')->with(['status'=>$status]);
    }
    public function hapus($id){
    	$pembeli=pembeli::find($id);
    	$pembeli->delete();
    	return redirect('pembeli');
    }
}
