<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\penulis;

class PenulisController extends Controller
{
    public function awal(){
    	$penulis=penulis::all();
    	return view('penulis.app',compact('penulis'));
    }
    public function tambah(){
    	return view('penulis.tambah');
    }

    public function simpan(Request $input){
    	$this->validate($input,array(
     'username' => 'required',
     'password' => 'required',
     'nama' => 'required',
     'notlp' => 'required',
     'email' => 'required',
     'alamat' => 'required',
));
            $penulis = new penulis();   
            $penulis->nama = $input->nama;   
            $penulis->notlp = $input->notlp;   
            $penulis->email = $input->email;   
            $penulis->alamat = $input->alamat;   
            $status = $penulis->save();
            return redirect('penulis')->with(['status'=>$status]);
    }
    public function edit($id){
    	$penulis=penulis::find($id);
    	return view('penulis.edit')->with(array('penulis'=>$penulis));

    }
    public function update($id,Request $input){
    	$penulis=penulis::find($id);
    	$penulis->nama=$input->nama;
    	$penulis->notlp=$input->notlp;
    	$penulis->email=$input->email;
    	$penulis->alamat=$input->alamat;
    	$status=$penulis->save();
    	return redirect('penulis')->with(['status'=>$status]);
    }
    public function hapus($id){
    	$penulis=penulis::find($id);
    	$penulis->delete();
    	return redirect('penulis');
    }
}
