<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\kategori;

class KategoriController extends Controller
{
    public function tambah(){
    	$kategori=kategori::all();
    	return view('buku.kategori',compact('kategori'));
    }

    public function simpan(Request $input){
        
 $this->validate($input, array   
        (             
            'deskripsi' => 'required|integer',          
            ));     
    	$kategori=new kategori();
    	$kategori->deskripsi=$input->deskripsi;
    	$kategori->save();
    	return redirect('kategori');
    }
    public function edit($id){
    	$kategori=kategori::find($id);
    	return view('buku.editkategori')->with(array('kategori'=>$kategori));
    }
      public function update($id,Request $input){
    	$kategori=kategori::find($id);
    	$kategori->deskripsi=$input->deskripsi;
    	$status=$kategori->save();
    	return redirect('kategori')->with(['status'=>$status]);
    }
    public function hapus($id){
    	$kategori=kategori::find($id);
    	$kategori->delete();
    	return redirect('kategori');
    }
}
