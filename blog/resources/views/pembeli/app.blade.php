@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Pembeli
		<div class="pull-right">
			Tambah Data <a href="{{ url('pembeli/tambah')}}" class="btn btn-success btn-xs">Tambah</a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Nama</td>
					<td>No Telepon</td>
					<td>E-mail</td>
					<td>Alamat</td>
				</tr>
				@foreach($pembeli as $Pembeli)
					
				<tr>
					<td >{{ $Pembeli->nama }}</td>
					<td >{{ $Pembeli->notlp}}</td>
					<td >{{ $Pembeli->email}}</td>
					<td >{{ $Pembeli->alamat}}</td>
					<td >
					
					<a href="{{url('pembeli/edit/'.$Pembeli->id)}}" class="btn btn-success btn-xs">Edit</a>
					<a href="{{url('pembeli/hapus/'.$Pembeli->id)}}"class="btn btn-success btn-xs">Hapus</a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection
